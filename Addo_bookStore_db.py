import sqlite3


def create_db():
    con = sqlite3.connect("Addo_O_Store.db")
    cur = con.cursor()
    cur.execute(
        "CREATE TABLE IF NOT EXISTS Book_Shelve (id INTEGER PRIMARY KEY , Title TEXT, Author TEXT, Year INTEGER, ISBN INTEGER)")
    con.commit()
    con.close()


def add_book(Title, Author, Year, ISBN):
    con = sqlite3.connect("Addo_O_Store.db")
    cur = con.cursor()
    cur.execute("INSERT INTO Book_Shelve VALUES (NULL , ?, ?, ?, ?)", (Title, Author, Year, ISBN))
    con.commit()
    con.close()


def selectAll_Records():
    con = sqlite3.connect("Addo_O_Store.db")
    cur = con.cursor()
    cur.execute("SELECT * FROM Book_Shelve")
    data = cur.fetchall()
    return data
    con.close()


def delete_book(id):
    con = sqlite3.connect("Addo_O_Store.db")
    cur = con.cursor()
    cur.execute("DELETE FROM Book_Shelve WHERE id=?", (id,))
    con.commit()
    con.close()


def update_book(id, Title, Author, Year, ISBN):
    con = sqlite3.connect("Addo_O_Store.db")
    cur = con.cursor()
    cur.execute("UPDATE Book_Shelve SET Title=?, Author=?, Year=?, ISBN=? WHERE id=?", (Title, Author, Year, ISBN, id))
    con.commit()
    con.close()


def search_book(Title="", Author="", Year="", ISBN=""):
    con = sqlite3.connect("Addo_O_Store.db")
    cur = con.cursor()
    cur.execute("SELECT * FROM Book_Shelve WHERE Title=? OR Author=? OR Year=? OR ISBN=?", (Title, Author, Year, ISBN))
    data = cur.fetchall()
    return data
    con.close()


create_db()
