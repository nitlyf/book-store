from tkinter import *
import Addo_bookStore_db
pa = ""
 # Application title
window = Tk(className = "Peter's Book Store")
window.configure(background="magenta2")

# input label for "TITLE"
title = Label(window, text="Title")
title.grid(row=0, column=0)
title_text = StringVar()
title_label = Entry(window, textvariable=title_text)
title_label.grid(row=0, column=1)
title.configure(background="maroon4")

# input label for "AUTHOR"
author = Label(window, text="Author")
author.grid(row=0, column=2)
author_text = StringVar()
author_label = Entry(window, textvariable=author_text)
author_label.grid(row=0, column=3)
author.configure(background="maroon4")

# input label for "YEAR"
year = Label(window, text="Year")
year.grid(row=1, column=0)
year_num = IntVar()
year_label = Entry(window, textvariable=year_num)
year_label.grid(row=1, column=1)
year.configure(background="maroon4")

# input label for "ISBN"
ISBN = Label(window, text="ISBN")
ISBN.grid(row=1, column=2)
ISBN_num = IntVar()
ISBN_label = Entry(window, textvariable=ISBN_num)
ISBN_label.grid(row=1, column=3)
ISBN.configure(background="maroon4")

# listbox scrollbar
scrollBar = Scrollbar(window)
scrollBar.grid(row=2, column=2, rowspan=6)
displayArea = Listbox(window, height=20, width=70)
displayArea.configure(yscrollcommand=scrollBar.set)
displayArea.grid(row=2, column=0, rowspan=6, columnspan=2)
scrollBar.configure(command=displayArea.yview)


# enbale user to get selected books
def selectedBook(event):
    global pa
    selected = displayArea.curselection()[0]
    pa = displayArea.get(selected)
    title_label.delete(0, END)
    title_label.insert(END, pa[1])
    author_label.delete(0, END)
    author_label.insert(END, pa[2])
    year_label.delete(0, END)
    year_label.insert(END, pa[3])
    ISBN_label.delete(0, END)
    ISBN_label.insert(END, pa[4])

displayArea.bind("<<ListboxSelect>>", selectedBook)

# Functions to add new book

def add_new_record():
    if len(title_text.get()) != 0:
        Addo_bookStore_db.add_book(title_text.get(), author_text.get(), year_num.get(), ISBN_num.get())

        displayArea.delete(0, END)
        view_records()


# Functions to view recorded entries

def view_records():
    displayArea.delete(0, END)
    for row in Addo_bookStore_db.selectAll_Records():
        displayArea.insert(END, row)


# Functions to update entries on the database

def update_entry():
    global pa
    if len(title_text.get()) != 0:
        Addo_bookStore_db.update_book(pa[0], title_text.get(), author_text.get(), year_num.get(), ISBN_num.get())
        view_records()


# Deleting function, enable user to delete existing data or entries

def delete_record():
    global pa
    if len(title_text.get()) != 0:
        Addo_bookStore_db.delete_book(pa[0])
        view_records()


# The search functions enable users to use any of the input to find an entry

def search():
    displayArea.delete(0, END)
    for row in Addo_bookStore_db.search_book(title_text.get() or author_text.get() or year_num.get() or ISBN_num.get()):
        displayArea.insert(END, row)
    

# The quit functions terminate the whole application process

def quit_app():
    window.destroy()


# View All Button"

allRecords = Button(window, text="View All", width=12, command=view_records)
allRecords.grid(row=2, column=3)

# Search Button
search = Button(window, text="Search Entry", width=12, command=search)
search.grid(row=3, column=3)

# Add Entry Button
add_entry = Button(window, text="Add New Entry", width=12, command=add_new_record)
add_entry.grid(row=4, column=3)

# Update Button
update = Button(window, text="Update Selected ", width=12, command=update_entry)
update.grid(row=5, column=3)

# Delete Button
delete_entry = Button(window, text="Delete Selected ", width=12, command=delete_record)
delete_entry.grid(row=6, column=3)

# Quit App Button
quitApp = Button(window, text="Close", width=12, command=quit_app)
quitApp.grid(row=7, column=3)


window.mainloop()
